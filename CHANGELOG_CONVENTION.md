# Changelog

Control of changes made in the different versions
Change description format based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
Version numbering based on [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

The versions will be listed from the most current to the oldest, at the end of the document there will be a list with references to all versions

* Version numbering **vA.B.CrD**:
+ **A** will indicate if there are new categories
+ **B** the incorporation of new courses
+ **C** minor changes, name or order, note taking within the course
+ **D** bug review, documentation, testing
+ **EXAMPLE** v0.0.1r0

## In the next version

### Added Features

### Fixes

## [UNRELEASED]

### Added Features
### Fixes

## EXAMPLE:

## [v0.0.1r0] - 2022-11-29

### Added

- Project creation
- Selection of version control and versioning methodology
- Selection of the collaboration methodology
- Selection of the type of license


[Unreleased]: https://gitlab.com/my-public-tools/pucher#
[v0.0.1r0]: https://gitlab.com/my-public-tools/pucher/-/tags/v0.0.1r0