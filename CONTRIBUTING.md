# How to contribute

In order to contribute you have to follow the following steps:
+ Create a branch with your update
+ When you have all the changes you want to add, request a merge
+ Respect the [code of conduct][code]

[code]: CODE_OF_CONDUCT.md