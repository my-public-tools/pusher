
is_error_rollback(){
    if [[ $IS_FAIL -eq $TRUE ]]; then
        print_info "Error in process then rollback"
        ./uninstall.bash
        exit 1
    fi
}

already_installed(){
    RESULT=$FALSE
    is_exist_directory_and_not_empty $INSTALL_DIR
    if [[ $RESULT -eq $TRUE ]]; then
       is_exist_link /usr/bin/pretty-hooks
    fi
}