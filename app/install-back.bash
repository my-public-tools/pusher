#!/bin/bash
# configs
source configs.bash

print_title "INSTALLER PRETTY-HOOKS `print_version $SOURCE_DIR`"

# check that permissions are root

# check already instalated
already_installed
if [[ $RESULT -eq $TRUE ]]; then
    print_info "Already installed then update application"
    ./update.bash
else
    ./install_internal.bash

    print_title FINISHED INSTALLATION
fi
