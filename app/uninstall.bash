#!/bin/bash
# configs
source configs.bash

print_title "UNINSTALLER PRETTY-HOOKS `print_version $INSTALL_DIR`"

# check that permissions are root

# remove link at directory in /usr/bin
check_command "unlink /usr/bin/pretty-hooks" "remove command successfully" "error in remove command"
# remove app at directory opt
check_command "[[ ! $INSTALL_DIR = '' ]] && [[ ! $INSTALL_DIR = '/opt' ]] && rm -rf $INSTALL_DIR" "remove files successfully" "error in remove files" 

print_title FINISHED DESINSTALLATION

exit 0