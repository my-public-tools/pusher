#!/bin/bash
source ./pusher/config.bash
# verify that user is not root
[[ $(id -u) -eq 0 ]] && echo "User is root, please use not root user" && exit 1
# verify not exsit file ~/.ssh/id_rsa
[[ ! -f ~/.ssh/id_rsa ]] && ssh-keygen
# copy ssh key at server
echo "COPY SSH KEY TO SERVER, PLEASE ENTER THE PASSWORD OF REMOTE SERVER $USER@$SERVER"
ssh-copy-id $USER@$SERVER
# copy files
echo "COPY FILES AT LOCAL OPT, PLEASE ENTER PASSWORD OF SUDO USER"
sudo cp -rf pusher /opt
# create aplication link
sudo ln -s /opt/pusher/pusher.bash /usr/bin/pusher
sudo ln -s /opt/pusher/pusher-flash.bash /usr/bin/pusher-flash
sudo ln -s /opt/pusher/pusher-restart-server.bash /usr/bin/pusher-restart-server
# add to app
cp "$CONFIG_APP" "$CONFIG_APP.back"
sed -i '/<\/toolSet>/d' "$CONFIG_APP"
echo "$EXTERNAL_TOOL" >> "$CONFIG_APP"
echo "</toolSet>" >> "$CONFIG_APP"
