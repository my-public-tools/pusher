. /opt/pusher/config.bash

if [ -z "$1" ]; then
    echo "DEFINE ONE PARAMETER PLEASSE"
    exit 1
fi

if [ "$1" = '-l' ]; then
    WORK_DIR="$LOCAL_DIR/$2"
else
    # calculate path $2 - $LOCAL_DIR
    LOCAL_ROUTE=$1
    LOCAL_ROUTE_WHITOUT_PREF=${LOCAL_ROUTE##*/expandweb3be/}
    LOCAL_ROUTE_WHITOUT_SUB=${LOCAL_ROUTE_WHITOUT_PREF%/*}
    REMOTE_DIR=$REMOTE_DIR/$LOCAL_ROUTE_WHITOUT_SUB
    WORK_DIR="$1"
fi

rsync -avh --update --delete "$WORK_DIR" "$USER@$SERVER:$REMOTE_DIR"
eval "$ACCION_FINALLY"
