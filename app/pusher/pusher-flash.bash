#/bin/bash
rsync -avh --update --delete $1 root@expandsandbox:/var/www/expand/web4/lib/python3.6/site-packages/expandweb3be
ssh -t root@expandsandbox "chown apache:apache -R /var/www/expand/web4/lib/python3.6/site-packages/expandweb3be/ ;service uwsgi restart; service httpd restart; service expandwebeg restart"
