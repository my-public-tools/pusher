USER=root
SERVER=expanddevel
INSTALL_DIR=/opt
LOCAL_DIR=/home/acheron/Documentos/Trabajo/eXpand/git/expandweb3/expandweb3be/
REMOTE_DIR=/var/www/expand/web4/lib/python3.6/site-packages/expandweb3be
ACCION_FINALLY="ssh -t root@expanddevel \"chown apache:apache -R \$REMOTE_DIR ;service uwsgi restart;\""
EXTERNAL_TOOL="<tool name=\"pusher\" description=\"Pushea al server de pruebas de eXpand\" showInMainMenu=\"false\" showInEditor=\"false\" showInProject=\"false\" showInSearchPopup=\"false\" disabled=\"false\" useConsole=\"true\">
    <exec>
      <option name=\"COMMAND\" value=\"pusher\" />
      <option name=\"PARAMETERS\" value=\"\$FilePath\$\" />
      <option name=\"WORKING_DIRECTORY\" value=\"\$ProjectFileDir\$\" />
    </exec>
  </tool>
  <tool name=\"pusher-restart-server\" description=\"Restart server de eXpand\" showInMainMenu=\"false\" showInEditor=\"false\" showInProject=\"false\" showInSearchPopup=\"false\" disabled=\"false\" useConsole=\"true\">
    <exec>
      <option name=\"COMMAND\" value=\"pusher-restart-server\" />
      <option name=\"PARAMETERS\" value=\"\$FilePath\$\" />
      <option name=\"WORKING_DIRECTORY\" value=\"\$ProjectFileDir\$\" />
    </exec>
  </tool>
"
CONFIG_APP="$HOME/.config/JetBrains/PyCharmCE2022.3/tools/External Tools.xml"