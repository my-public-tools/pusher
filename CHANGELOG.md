# Changelog

## In the next version

### Added Features
+ Add a selectable configuration file
+ Add configurable prompts in which the actions to be performed are set
+ Allow pushing the entire project to replace it
+ Allow pushing all modified files in the repository
+ Allow pushing a directory

## [v0.0.1r0] - 2024-10-10

### Added

- Project creation
- Selection of version control and versioning methodology
- Selection of the collaboration methodology
- Selection of the type of license


[Unreleased]: https://gitlab.com/my-public-tools/easy-configs#
[v0.0.1r0]: https://gitlab.com/my-public-tools/easy-configs/-/tags/v0.0.1