<div align="center">
  <a title="Code" href="#" target="_blank">
    <img alt="Code" src="https://img.shields.io/static/v1?label=code&message=PERL&color=blue&style=flat-square&logo=gnubash&logoColor=white" />
  </a>
  <a title="Lisence" href="https://gitlab.com/my-public-tools/pretty-hooks/-/blob/main/LICENSE" target="_blank">
    <img alt="Lisence" src="https://img.shields.io/static/v1?label=lisence&message=GPL3&color=blue&style=flat-square&logo=unlicense&logoColor=white" />
  </a>
  <a title="Stage" href="#" target="_blank">
    <img alt="Stage" src="https://img.shields.io/static/v1?label=stage&message=unstable&color=orange&style=flat-square&logo=undertale&logoColor=white" />
  </a>

</div>

# PUSHER
Application for uploading files and executing code on remote servers
## Installation

Instruction for installing:

```bash
git clone https://gitlab.com/my-public-tools/pusher.git
cd app
sudo ./install.bash
```

## Check ultimate version

Instruction for installing:

```bash
git describe --tags $(git rev-list --tags --max-count=1)
```

## Usage

To use it externally:

```bash
# UPLOAD FILE
pusher file
```

## License
This repository is under the gnu GPL3 license, you can check it here [license][license]

## Change control
You can follow the changes in the different versions in our [changelog][changelog]

## Code of Conduct
To belong to our community you must take into account the following [code of conduct][code]

## How to contribute
If you would like to contribute, you can do so by following these tips [how to contribute][contribute]

## How to write commit messages
We will follow the following convention to write commits
[commit-summary][commit-summary] [commit][commit]

## Authors
These are the main authors and contributors to the project [authors][authors]


[license]:LICENSE
[authors]: AUTHORS.md
[changelog]: CHANGELOG.md
[code]: CODE_OF_CONDUCT.md
[contribute]: CONTRIBUTING.md
[commit-summary]: COMMIT_CONVENTION_SUMMARY.md
[commit]: COMMIT_CONVENTION.md


### External links
https://www.contributor-covenant.org/translations/
https://www.conventionalcommits.org/en/v1.0.0/#specification